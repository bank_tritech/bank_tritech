-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 01 Feb 2019 pada 13.57
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_tritech`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyetoran`
--

CREATE TABLE `penyetoran` (
  `id_penyetoran` int(50) NOT NULL,
  `validasi` varchar(100) NOT NULL,
  `tabungan` varchar(100) NOT NULL,
  `no_rekening` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nomor_hp` varchar(100) NOT NULL,
  `cabang` varchar(100) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tunai` int(50) NOT NULL,
  `nilai` int(50) NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `total` int(50) NOT NULL,
  `terbilang` varchar(100) NOT NULL,
  `nomor_identitas` varchar(100) NOT NULL,
  `tll` varchar(100) NOT NULL,
  `usaha` varchar(100) NOT NULL,
  `penghasilan` int(50) NOT NULL,
  `sumber_dana` varchar(100) NOT NULL,
  `tujuan_penggunaan` varchar(100) NOT NULL,
  `hubungan_rek` varchar(100) NOT NULL,
  `warga_negara` varchar(100) NOT NULL,
  `npwp` int(50) NOT NULL,
  `jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyetoran`
--

INSERT INTO `penyetoran` (`id_penyetoran`, `validasi`, `tabungan`, `no_rekening`, `nama`, `alamat`, `nomor_hp`, `cabang`, `tanggal`, `tunai`, `nilai`, `nomor`, `bank`, `total`, `terbilang`, `nomor_identitas`, `tll`, `usaha`, `penghasilan`, `sumber_dana`, `tujuan_penggunaan`, `hubungan_rek`, `warga_negara`, `npwp`, `jabatan`) VALUES
(1, '', 'Giro Wadiah', '21313', 'wqeq', 'qweqq', '082274283616', 'medan', '2019-01-30 17:00:00', 0, 40000, 'q342423', '', 232131, 'lima puluh ribu rupiah', '21313', 'iohjokhnnlkn', 'wi', 2147483647, 'perkejeraan', 'makan', 'punya sendiri', 'WNI', 32424324, 'direktur'),
(2, '', 'Giro Wadiah', '21313', 'wqeq', 'qweqq', '082274283616', 'medan', '2019-01-30 17:00:00', 0, 40000, 'q342423', '', 232131, 'lima puluh ribu rupiah', '21313', 'iohjokhnnlkn', 'wi', 2147483647, 'perkejeraan', 'makan', 'punya sendiri', 'WNI', 32424324, 'direktur'),
(3, '', 'Giro Wadiah', '21313', 'wqeq', 'qweqq', '082274283616', 'medan', '2019-01-30 17:00:00', 0, 40000, 'q342423', '', 232131, 'lima puluh ribu rupiah', '21313', 'iohjokhnnlkn', 'wi', 2147483647, 'perkejeraan', 'makan', 'punya sendiri', 'WNI', 32424324, 'direktur'),
(4, '', 'Giro Wadiah', '21313', 'wqeq', 'qweqq', '082274283616', 'medan', '2019-01-30 17:00:00', 0, 40000, 'q342423', '', 232131, 'lima puluh ribu rupiah', '21313', 'iohjokhnnlkn', 'wi', 2147483647, 'perkejeraan', 'makan', 'punya sendiri', 'WNI', 32424324, 'direktur'),
(5, '', 'Giro Wadiah', '21313', 'wqeq', 'qweqq', '082274283616', 'medan', '2019-01-30 17:00:00', 0, 40000, 'q342423', '', 232131, 'lima puluh ribu rupiah', '21313', 'iohjokhnnlkn', 'wi', 2147483647, 'perkejeraan', 'makan', 'punya sendiri', 'WNI', 32424324, 'direktur'),
(6, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(7, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(8, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(9, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(10, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(11, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(12, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(13, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(14, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(15, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(16, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(17, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(18, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(19, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(20, '', 'tabungan Mudharabah', '213131', '21313', 'weq', '082274283616', 'medan', '2019-01-30 17:00:00', 21313, 2131, '21313', '', 342342, 'lima puluh ribu rupiah', 'q', 'b', 'c', 0, 'd', 'f', 'h', 'WNI', 32424324, 'direktur'),
(21, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(22, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(23, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(24, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(25, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(26, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(27, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(28, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(29, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(30, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(31, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(32, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(33, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(34, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(35, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(36, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(37, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(38, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(39, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(40, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(41, '', 'Deposito Mudharabah', '0', '', '', '', '', '2019-01-30 17:00:00', 0, 40000, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(42, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(43, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(44, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(45, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(46, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(47, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 200000, '', '', '', '', 0, '', '', '', '', 0, ''),
(48, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(49, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(50, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(51, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(52, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(53, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(54, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(55, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(56, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(57, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(58, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(59, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(60, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(61, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(62, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(63, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(64, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(65, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(66, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(67, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(68, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(69, '', 'Deposito Mudharabah', '1', '', '', '', '', '2019-01-30 17:00:00', 0, 0, '', '', 400000, '', '', '', '', 0, '', '', '', '', 0, ''),
(70, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(71, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(72, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(73, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(74, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(75, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(76, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(77, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(78, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(79, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(80, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(81, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(82, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(83, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(84, '', 'tabungan Mudharabah', '1', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(85, '', 'tabungan Mudharabah', '0', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 500000, '', '', '', '', 0, '', '', '', '', 0, ''),
(86, '', 'Deposito Mudharabah', '0', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(87, '', 'Deposito Mudharabah', 'Array', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 0, '', '', '', '', 0, '', '', '', '', 0, ''),
(88, '', 'tabungan Mudharabah', 'Array', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(89, '', 'tabungan Mudharabah', '2019 3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(90, '', 'tabungan Mudharabah', '2019,3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(91, '', 'tabungan Mudharabah', '3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(92, '', 'tabungan Mudharabah', '3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(93, '', 'tabungan Mudharabah', '3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, ''),
(94, '', 'tabungan Mudharabah', '2019,3', '', '', '', '', '2019-01-31 17:00:00', 0, 0, '', '', 123131, '', '', '', '', 0, '', '', '', '', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `penyetoran`
--
ALTER TABLE `penyetoran`
  ADD PRIMARY KEY (`id_penyetoran`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `penyetoran`
--
ALTER TABLE `penyetoran`
  MODIFY `id_penyetoran` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
