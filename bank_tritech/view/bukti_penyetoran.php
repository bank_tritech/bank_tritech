<?php
$page = 'penyetoran';
include 'navbar.php';
?>

<div class="row">
	<div class="col-6 float-left my-border">
		<input type="text" name="" value="" placeholder="" class="my-input">
	</div>
	<div class="col-6 float-left my-border-right">
		<div class="row">
			<div class="col-4">
				
			</div>
			<div class="col-6">
				<p>Cabang .....................................</p>
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				
			</div>
			<div class="col-2">
			</div>
			<div class="col-6">
				<label>
					<p>Tanggal</p>
				</label>
				<input type="date" name="" value="" placeholder="" class="date">
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6 float-left my-border">
		<div class="row">
			<div class="col-6">
				<input type="checkbox" name="" value="" class="input-bot"> Giro Wadiah
			</div>
			<div class="col-6">
				<input type="checkbox" name="" value="" class="input-bot"> Tabungan Mudharabah
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="checkbox" name="" value="" class="input-bot"> Tabungan Wadiah
			</div>
			<div class="col-6">
				<input type="checkbox" name="" value="" class="input-bot"> Giro Mudharabah
			</div>
		</div>
		<div class="row">
			<div class="col-5 my-border-border">
				<p>No. Rekening</p>
			</div>
			<div class="col-7 my-border-border">
				<input type="number" name="" value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-5 my-border-border-top">
				<p>Nama Pemilik rekening</p>
				<p>Alamat dan No. telepon</p>
			</div>
			<div class="col-7 my-border-border-top">
				<input type="text" name="" value="" placeholder=""><br>
				<input type="tel" name="" value="" placeholder=""><br>
				<input type="address" name="" value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h5>Wajib diisi penyetor untuk setoran diatas Rp. 500 juta</h5>
				<p>Nomor Identitas: ....................................................................................... KTP/SIM/Passport </p>
				<p>Tempat/tgl.Lahir : ........................................................ Warga Negara : ..................................</p>
				<p>Pekerjaan/Bidang Usaha : ........................................................ NPWP : ..................................</p>
				<p>Jlh. Penghasilan/bulan: ........................................................ Jabatan : ..................................</p>
				<p>Sumber Dana .........................................................................................</p>
				<p>Tujuan Penggunaan ...............................................................................</p>
				<p>Hubungan dengan rekening .......................................................................................................</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6 my-border-border-bot">
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<p>Tanda Tangan Teller</p>
			</div>
			<div class="col-6">
				<p>Disetor Oleh</p>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<p>Tanda Tangan Penyetor</p>
			</div>
		</div>
	</div>
	<div class="col-6 float-left my-border">
		<table width="100%" border="1px solid black">
			<tr>
				<td>
					<p>Tunai</p>
				</td>
				<td>
					<p>Rp</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Cek/Bilyet Giro</p>
					<table width="100%" class="my-table" border="1px solid black">
						<tr>
							<td>
								<p>Nomor</p>
							</td>
							<td>
								<p>Bank</p>
							</td>
						</tr>
						<tr>
							<td>
								<p></p>
							</td>
							<td>
								<p></p>
							</td>
						</tr>
						<tr>
							<td>
								<p></p>
							</td>
							<td>
								<p></p>
							</td>
						</tr>
						<tr>
							<td>
								<p></p>
							</td>
							<td>
								<p></p>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<p>Total</p>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<h5>Total Kredit</h5>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table width="100%" border="1px solid black">
						<tr>
							<td colspan="2"><p></p></td>
						</tr>
						<tr>
							<td colspan="2"><p></p></td>
						</tr>
						<tr>
							<td colspan="2"><p></p></td>
						</tr>
						<tr>
							<td colspan="2"><p></p></td>
						</tr>
						<tr>
							<td colspan="2"><p></p></td>
						</tr>
						<tr>
							<td colspan="2">
								<h5>RP.</h5>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div class="row">
			<div class="col-3">
				<p>Terbilang</p>
			</div>
			<div class="col-3">
			</div>
			<div class="col-6">
				<input type="text" name="" value="" placeholder="" class="input-bot-bot"><br>
				<input type="text" name="" value="" placeholder="" class="input-bot-bot"><br>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table table-bordered">
					<thead class="thead-dark">
						<tr>
							<th scope="col" colspan="3" class="text-center">Diisi Oleh Bank</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Kurs</td>
							<td></td>
							<td>Rp.</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
			<div class="row">
				<div class="col-7">
					<b>Transaksi Dianggap Sah Bila Bukti Setoran ini Dibubuhi Tanda Tangan dan Validasi Teller</b>
				</div>
				<div class="col-5 text-right">
					<p>Nota Kredit</p>
				</div>
			</div>