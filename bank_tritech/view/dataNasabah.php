<?php 
    $page = 'dataNasabah';
    include 'navbar.php';
 ?>

<div class="container mt-3">
    <div class="row mt-3">
        <form action="">
            <div class="input-group mb-3">
                <input type="number" class="form-control" placeholder="masukkan rekening" name="search">
                <div class="input-group-append">
                    <button class="btn btn-outline-primary ml-3" type="button">Search</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Rekening</th>
                    <th>Saldo</th>
                    <th>Aksi</th>
                </tr>   
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Ardika Kuswahyudi</td>
                    <td>012032919</td>
                    <td>Rp 5.000.000.000</td>
                    <td>
                        <a href="#" class="badge badge-primary">View</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>