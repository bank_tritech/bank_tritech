<?php 
    $page = 'buka_rekening';
    include 'navbar.php';
?>
<div class="container">
<div id="carouselExampleControls" class="carousel slide" data-interval="false">
    <div class="carousel-inner">
        <div class="carousel-item active" alt="slide-1">
            <div class="card col-md-10 offset-md-1">
                <form action="">
                    <div class="card-body">
                        <h5 class="card-title text-center">Formulir Pembukaan Rekening</h5><hr>
                            <table width="100%">
                                <tr>
                                    <th>Nasabah</th>
                                    <td>
                                        <input type="radio" name="baru" value="1">
                                        <label for="baru">Baru</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="baru" value="2">
                                        <label for="baru">Lama</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Bertindak Untuk</th>
                                    <td>
                                        <input type="radio" name="untuk" value="1">
                                        <label for="baru">Diri Sendiri</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="untuk" value="2">
                                        <label for="baru">Pihak Yang Diwakili</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Cabang</th>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <td>
                                        <input type="date" class="form-control">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- Identitas Nasabah -->
                        <hr><h5 class="card-title text-center ">Identitas Nasabah</h5><hr>
                                <table>
                                    <tr>
                                        <th>Nama Sesuai Identitas</th>
                                        <td>
                                            <input type="text" name="nama" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Identitas</th>
                                        <td>
                                            <input type="radio" name="jenis">KTP
                                            <input type="radio" name="jenis" class="ml-4">SIM
                                            <input type="radio" name="jenis" class="ml-4">PASPOR
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="radio" name="jenis" class="ml-4">IDENTITAS
                                            <input type="radio" name="jenis"s class="ml-4">lainnya
                                        </td>
                                        <td> 
                                            <input type="text" class="form-control" name="lainnya">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>No. Identitas</th>
                                        <td><input type="text" class="form-control" name="nomor" required></td>
                                    </tr>
                                    <tr>
                                        <th>Berlaku Hingga</th>
                                        <td>
                                            <input type="date" class="form-control">
                                        </td>
                                        <td>
                                            <input type="checkbox" class="ml-3"> Seumur Hidup
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status Kependudukan</th>
                                        <td>
                                            <input type="radio" name="kependudukan"> Penduduk
                                        </td>
                                        <td>
                                            <input type="radio" name="kependudukan" class="ml-4"> Non Penduduk
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Nama Alias(Jika Ada)</th>
                                        <td><input type="text" class="form-control" name="alias"></td>
                                    </tr>
                                    <tr>
                                        <th>Kewarganegaraan</th>
                                        <td>
                                            <input type="radio" name="kewarganegaraan" valui="1"> WNI
                                        </td>
                                        <td>
                                            <input type="radio" name="kewarganegaraan" class="ml-4" valur="2"> WNA
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status Perkawinan</th>
                                        <td>
                                            <input type="radio" name="perkawinan"> Belum Menikah
                                        </td>
                                        <td>
                                            <input type="radio" name="perkawinan" class="ml-4"> Menikah
                                        </td>
                                        <td>
                                            <input type="radio" name="perkawinan" class="ml-4"> Janda/Duda
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Agama
                                        </th>
                                        <td>
                                            <input type="radio" name="agama"> Islam
                                            <input type="radio" name="agama" class="ml-4"> Kristen
                                        </td>
                                        <td>
                                            <input type="radio" name="agama" class="ml-4"> Katolik
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="radio" name="agama" class="ml-4"> Hindu
                                            <input type="radio" name="agama" class="ml-4"> Budha
                                        </td>
                                        <td>
                                            <input type="radio" name="agama" class="ml-4"> Kong Hu Chu
                                            <input type="radio" name="agama" class="ml-1">lainnya
                                        </td>
                                        <td> 
                                            <input type="text" class="form-control" name="lainnya">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Nama Ibu Kandung</th>
                                        <td>
                                            <input type="text" name="nama_ibu" class="form-control" required>
                                        </td>
                                    </tr>
                                </table>
                        <!-- akhir identitas -->

                        <!-- data nasabah -->
                        <hr><h5 class="card-title text-center ">Data Nasabah</h5><hr>
                                <table width="100%">
                                    <tr>
                                        <th>NPWP</th>
                                        <td>
                                            <input type="text" name="npwp" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>TIN/SSN (Jika Ada)</th>
                                        <td>
                                            <input type="text" name="tin" class="form-control" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Tinggal Sekarang (diisi jika berbeda dengan kartu identitas)</th>
                                        <td>
                                            <textarea name="alamat" class="form-control"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Desa/Kelurahan</th>
                                        <td><input type="text" class="form-control" name="desa" required></td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kabupaten</th>
                                        <td><input type="text" class="form-control" name="kota" required></td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td><input type="text" class="form-control" name="provinsi" required></td>
                                    </tr>
                                    <tr>
                                        <th>Kode Pos</th>
                                        <td><input type="text" class="form-control" name="kodepos" required></td>
                                    </tr>
                                    <tr>
                                        <th>Pendidikan Sampai Dengan</th>
                                        <td>
                                            <input type="radio" name="pendidikan"> SMP
                                            <input type="radio" name="pendidikan" class="ml-3"> SMU
                                            <input type="radio" name="pendidikan" class="ml-3"> Diploma
                                        </td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                        <td>
                                            <input type="radio" name="pendidikan"> S1
                                            <input type="radio" name="pendidikan" class="ml-3"> S2
                                            <input type="radio" name="pendidikan" class="ml-3"> S3
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Email</th>
                                        <td>
                                            <input type="email" name="email" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Telepon Selular</th>
                                        <td>
                                            <input type="text" name="selular" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Telepon Rumah</th>
                                        <td>
                                            <input type="text" name="npwp" class="form-control" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Tinggal Di Negara lain (Apabila ada)</th>
                                        <td>
                                            <textarea name="alamat" class="form-control"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kabupaten</th>
                                        <td><input type="text" class="form-control" name="kota"></td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td><input type="text" class="form-control" name="provinsi"></td>
                                    </tr>
                                    <tr>
                                        <th>Kode Pos</th>
                                        <td><input type="text" class="form-control" name="kodepos"></td>
                                    </tr>
                                </table>
                        <!-- akhir nasabah -->
                        <a  href="#carouselExampleControls" role="button" data-slide="next">
                            <button name="next1" type="button" class="btn btn-outline-info mt-3" style="margin-left:80%;">Next</button>
                        </a>
                </form>
            </div>
        </div>

        <!-- Slide 2 -->
        <div class="carousel-item">
            <div class="card col-md-12" alt="slide-2">
                <form action="">
                    <div class="card-body">
                        <h5 class="card-title text-center">Data Pekerjaan</h5><hr>
                            <table width="110%">
                                <tr>
                                    <th>Status Pekerjaan</th>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="1">
                                        <label for="baru">Karyawan Bank</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="2">
                                        <label for="baru">Karyawan Swasta</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="3">
                                        <label for="baru">PNS/TNI/POLRI/BUMN/BUMD</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="4">
                                        <label for="baru">Ibu Rumah Tangga</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="5">
                                        <label for="baru">Pelajar</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="6">
                                        <label for="baru">Pendidik/Guru</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="7">
                                        <label for="baru">Wirausaha</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="pekerjaan" value="8">
                                        <label for="baru">Lainnya</label>
                                        <input type="text" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Institusi/Perusahaan</th>
                                    <td>
                                        <input type="text" name="institusi" class="form-control">
                                    </td>
                                    <td class="text-right">
                                        <label>Jabatan</label>
                                    </td>
                                    <td>
                                        <input type="text" name="institusi" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Bidang Pekerjaan</th>
                                    <td>
                                        <input type="radio" name="bidang" value="1">
                                        <label>Keuangan</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="bidang" value="2">
                                        <label>Jasa Nonkeuangan</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="bidang" value="3">
                                        <label>Manufaktur</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="bidang" value="4">
                                        <label>Perdagangan</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="radio" name="bidang" value="5">
                                        <label>Pemerintahan</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="bidang" value="6">
                                        <label>Pelayanan Publik</label>
                                    </td>
                                    <td>
                                        <input type="radio" name="bidang" value="7">
                                        <label>Pengawasan</label>
                                    </td>
                                        <td>
                                            <input type="radio" name="bidang" value="8">
                                            <label>Lainnya</label>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Kantor</th>
                                        <td>
                                            <textarea name="alamat" cols="20" class="form-control"></textarea>
                                        </td>
                                        <td class="text-right">
                                            <label>Telepon Kantor</label>
                                        </td>
                                        <td>
                                            <input type="text" name="telp_kantor" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kode Pos</th>
                                        <td>
                                            <input type="text" class="form-control" name="kode pos">
                                        </td>
                                        <td class="text-center">
                                            <label>Alamat Surat Menyurat</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="bidang" value="1">
                                            <label>Alamat Identitas</label>
                                        </td>
                                        <td >
                                            <input type="radio" name="bidang" value="2">
                                            <label>Alamat Kantor</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td>
                                            <input type="radio" name="bidang" value="3">
                                            <label>Alamat Tinggal Sekarang</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Penghasilan Tetap Per Bulan</th>
                                        <td>
                                            <input type="radio" name="tetap" value="1">
                                            <label> < Rp10 juta </label>
                                        </td>
                                        <td>
                                            <input type="radio" name="tetap" value="2">
                                            <label>Rp10 juta < Rp50 juta</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="tetap" value="3">
                                            <label> >= Rp50 juta </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Penghasilan Tidak Tetap Per Bulan</th>
                                        <td>
                                            <input type="radio" name="tidak" value="1">
                                            <label> < Rp10 juta </label>
                                        </td>
                                        <td>
                                            <input type="radio" name="tidak" value="2">
                                            <label>Rp10 juta < Rp50 juta</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="tidak" value="3">
                                            <label> >= Rp50 juta </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Pengeluaran Tetap Per Bulan</th>
                                        <td>
                                            <input type="radio" name="keluar" value="1">
                                            <label> < Rp10 juta </label>
                                        </td>
                                        <td>
                                            <input type="radio" name="keluar" value="2">
                                            <label>Rp10 juta < Rp50 juta</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="keluar" value="3">
                                            <label> >= Rp50 juta </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Informasi Penghasilan Tambahan</th>
                                        <td>
                                            <input type="radio" name="informasi" value="1">
                                            <label>Kerja Paruh Waktu</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="informasi" value="2">
                                            <label>Hasil Usaha</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="informasi" value="3">
                                            <label>Hasil Sewa</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="informasi" value="4">
                                            <label>Dividen</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="radio" name="informasi" value="5">
                                            <label>Investasi</label>
                                        </td>
                                        <td>
                                            <input type="radio" name="informasi" value="6">
                                            <label>Warisan</label>
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="informasi" value="7">
                                            <label>Lainnya</label>
                                            <input type="text" name="informasi" class="form-control">
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                            </table>
                        </div><hr>
                        <a  href="#carouselExampleControls" role="button" data-slide="next">
                            <button type="button" class="btn btn-outline-info mt-3" style="margin-left:80%;">Next</button>
                        </a>
                </form>
            </div>
        </div>

        <!-- slide terakhir -->
            <!-- Slide 2 -->
        <div class="carousel-item">
            <div class="card col-md-12" alt="slide-4">
                <form action="">
                    <div class="card-body">
                    <h5></h5>
                        <hr>
                            <ol start="8">
                                <li>
                                    Sesuai Ketentuan yang berlaku mengenai Prinsip Mengenal Nasabah bahwa setiap calon Nasabah/Nasabah wajib menyerahkan Nomor Pokok Wajib Pajak(NPWP). Sebagai salah satu persyaratan pembukaan rekening di Bank. Dalam hal Nasabah hanya membuka rekening Tabungan atau deposito dan belum dapat menyerahkan NPWP kepada BANK. maka Nasabah Menyatakan sebagai berikut:<br>

                                    <div class="cb1">
                                        <input type="checkbox" name="">
                                    </div>
                                    <div class="cb2">
                                        Saya adalah Wajib Pajak yang memperoleh penghasilan melebihi Penghasilan Tidak Kena Pajak (PTKP) namun hingga saat ini belum dapat menyerahkan NPWP kepada Bank
                                    </div>

                                    <br><br>

                                    <div class="cb1">
                                        <input type="checkbox" name="">
                                    </div>
                                    <div class="cb2">
                                        Saya tidak diwajibkan mendaftarkan diri sebagai Wajib Pajak dengan alasan (pilih salah satu):
                                    </div>

                                    <div class="cbb1">
                                        <div class="cb1">
                                            <input type="checkbox" name="">
                                        </div>
                                        <div class="cb2">
                                            Memiliki Penghasilan yang tidak melrbihi Penghasilan Tidak Kena Pajak (PTKP)
                                        </div>

                                        <div class="cb1">
                                            <input type="checkbox" name="">
                                        </div>
                                        <div class="cb2">
                                            Tidak bekerja/tidak memiliki penghasilan
                                        </div>

                                        <div class="cb1">
                                            <input type="checkbox" name="">
                                        </div>
                                        <div class="cb2">
                                            Lainnya <input type="text" name="">
                                        </div>
                                    </div>
                                    Apabila di kemudian hari saya memiliki NPWP atau telah memenuhi persyaratan sebagai Wajib Pajak sesuai dengan ketentuan Peraturan      Perundang-Undangan yang berlaku. maka saya akan menyerahkan NPWP tersebut kepada Bank
                                </li>
                                <br>
                                <li>
                                    Jika terdapat perubahan data atau informasi termasuk tapi tidak terbatas perubahan data nomor telepon seluler dan/atau informasi lainnya yang telah saya berikan kepada Bank. maka saya akan memberitahukan perubahan data atau informasi tersebut kepada Bank dalam jangka waktu selambat-lambatnya 30 (tiga puluh) hari kalender terhitung sejak terjadinya perubahan perubahan tersebut atau dalam jangka waktu lain yang akan diberitahukan oleh Bank kepada Nasabah dalam bentuk dan melalui sarana apa pun.
                                </li>
                                <br>
                                <li>
                                    Setuju bahwa PT. Bank Tritech Syariah indonesia. Tbk berhak mengakhiri hubungan dengan Nasabah jika di kemudian hari dapat dibuktikan bahwa saya telah memberikan data atau informasi yang tidak benar, tidak akurat, atau tidak lengkap, atau saya tidak memberitahukan perubahan data atau informasi terkait Nasabah kepada Bank Tritech Syariah dalam jangka waktu ditentukan.
                                </li>
                                <br>
                                <li>
                                    Nasabah dengan ini menyatakan:
                                    <div class="s">
                                        <div class="cb1">
                                            <input type="checkbox" name="">
                                        </div>
                                        <div class="cb2">
                                            SETUJU
                                        </div>
                                    </div>
                                    <div class="ts">
                                        <div class="cb1">
                                            <input type="checkbox" name="">
                                        </div>
                                        <div class="cb2">
                                            TIDAK SETUJU
                                        </div>
                                    </div>
                                    Bank menggunakan dan memberikan data dan/atau informasi pribadi saya kepada pihak lain yang bekerja sama dengan Bank. serta menerima informasi produk dan atau layanan melalui sarana komunikasi pribadi pada jam kerja atau di luar jam kerja.
                                </li>
                            </ol>
                            <b>Tambahan Khusus Layanan Informasi Elektronik</b><br><br>
                            <ol start="12">
                                <li>Dengan ini saya bersedia mengaktifkan fasilitas roaming dari kartu telepon seluler saya untuk dapat menikmati fasilitas SMS notifikasi saat saya berada di luar negeri;</li><br>
                                <li>Apabila saya tidak mengisi pilihan nominal transaksi pada layanan notifikasi yang dikehendaki untuk menerima layanan ini, maka dianggap saya       mengikuti ketentuan default layanan yang berlaku;</li><br>
                                <li>
                                Bersedia ditutupnya layanan notifikasi secara otomatis apabila biaya atas notifikasi tersebut tidak dapat ditagihkan kepada rekening Nasabah       dikarenakan saldo tidak cukup (di bawah saldo minimum ketentuan produk).
                                </li>
                            </ol>
                            Dengan ditandatanganinya Fomulir Pembukaan Rekening Perorangan ini. saya menyatakan bahwa seluruh data isian dan pilihan yang saya berikan adalah benar dan saya menyatakan tunduk pada ketentuan produk/atau layanan yang berlaku berikut perubahannya di kemudian hari sebagaimana yang diberitahukan oleh PT. Bank Tritech Syariah Indonesia. Tbk melalui jaringan Kantor/surat/email/media lainnya serta tunduk dan terikat pada peraturan yang berlaku di PT. Bank Tritech Syariah Indonesia. Tbk Bank Indonesia(BI)/Otoritas Jasa Keuangan(OJK), dan fatwa Dewan Syariah Nasional(DSN) yang merupakan satu kesatuan dan bagian yang tidak terpisahkan di fomulir ini.
                            <br><br>
                            <div class="ttgn">
                                <p style="text-align: center;">Para Pihak</p>
                                <div class="tbl1">
                                    <table border="1" width="45%"  align="left">
                                        <tr>
                                            <td style="text-align: center;">
                                                <span>BANK</span><br>
                                                <i>Bank</i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <span>TANGGAL</span><br>
                                                            <i>Date</i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <span>TANDA TANGAN</span><br>
                                                            <i>Signature</i>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </table>
                                </div>

                                <div class="tbl1">
                                    <table border="1" width="45%"  align="right">
                                        <tr>
                                            <td style="text-align: center;">
                                                <span>NASABAH</span><br>
                                                <i>Costumer</i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <span>TANGGAL</span><br>
                                                            <i>Date</i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <span>TANDA TANGAN</span><br>
                                                            <i>Signature</i>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                    </div>
                    </div>




                <!-- card 2 -->





                <div class="col-md-12 card">
                    <div class="card-body">
                            <h5>Untuk Keperluan Bank dan Validasi/For Bank and Validation</h5>
                        <hr>

                        <div class="form1">
                            <div class="namaform1">
                                <span>NAMA REKENING</span><br>
                                <i>Account name</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NOMOR REKENINNG</span><br>
                                <i>Account number</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NOMOR NASABAH</span><br>
                                <i>Costumer name</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>KODE CABANG</span><br>
                                <i>Branch code</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NOMOR NASABAH JOINT OR/AND</span><br>
                                <i>Joint Or/And customer number</i>
                            </div>
                            <div class="inputform11">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>HUBUNGAN NASABAH DENGAN PIHAK BANK</span><br>
                                <i>Customer relation with bank</i>
                            </div>
                            <div class="inputform11">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NAMA PRODUK</span><br>
                                <i>Product name</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>KODE PRODUK</span><br>
                                <i>Product code</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>KODE MARKETING</span><br>
                                <i>Marketing code</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>KODE REFERENSI</span><br>
                                <i>Refferal code</i>
                            </div>
                            <div class="inputform1">
                                <input type="text" name="">
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NASABAH TERKAIT FATCA</span><br>
                                <i>Related FATCA</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">YA <br>
                                <i>Yes</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">TIDAK <br>
                                <i>No</i>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>NASABAH TERKAIT CRS &emsp;</span><br>
                                <i>Related CRS</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">YA <br>
                                <i>Yes</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">TIDAK <br>
                                <i>No</i>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="form1">
                            <div class="namaform1">
                                <span>PEPS</span><br>
                                <i>Politically Exposed Persons</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">YA <br>
                                <i>Yes</i>
                            </div>
                            <div class="inputform12">
                                <input type="checkbox" name="">TIDAK <br>
                                <i>No</i>
                            </div>
                            <br><br><br>

                            <table border="1" width="80%">
                                <tr>
                                    <td style="text-align: center;" width="33.3%">
                                        <span>BANK</span><br>
                                        <i>Bank</i>
                                    </td>
                                    <td style="text-align: center;" width="33.3%">
                                        <span>DISETUJUI OLEH</span><br>
                                        <i>Approved by</i>
                                    </td>
                                    <td width="33.3%">
                                        <span>Screening dan diproses oleh</span><br>
                                        <i>Screening and processed by</i>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <br><br><br><br><br><br><br><br><br><br><br>
                                        <p>TTD dan nama lengkap</p>
                                    </td>
                                    <td style="text-align: center;">
                                        <br><br><br><br><br><br><br><br><br><br><br>
                                        <p>TTD dan nama lengkap</p>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="">UN List/blacklist lainnya <br>
                                        <input type="checkbox" name="">DHN List(Khusus Giro)
                                        <br><br><br><br><br><br><br><br><br><br>
                                        <p style="text-align: center;">TTD dan nama lengkap</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        </div>
                        <a  href="#carouselExampleControls" role="button" data-slide="next">
                            <button type="button" class="btn btn-outline-info mt-3" style="margin-left:80%;">Next</button>
                        </a>
                </form>
            </div>
        </div>
        <!-- akhir -->
    </div>
</div>
</div>

<script type="text/javascript">
    function verifyFirst() {
        // console.log("clicked");
        // ajax kan ke url /buka_regenig pake post
    }
</script>