<?php
    $page = 'home';
    include 'navbar.php';
?>
<!-- card 1 -->
<div class="container">
   <div class="col-md-12 card">
      <div class="card-body">
            <h5></h5>
        <hr>
            <ol start="8">
                <li>
                     Sesuai Ketentuan yang berlaku mengenai Prinsip Mengenal Nasabah bahwa setiap calon Nasabah/Nasabah wajib menyerahkan Nomor Pokok Wajib Pajak(NPWP). Sebagai salah satu persyaratan pembukaan rekening di Bank. Dalam hal Nasabah hanya membuka rekening Tabungan atau deposito dan belum dapat menyerahkan NPWP kepada BANK. maka Nasabah Menyatakan sebagai berikut:<br>

                    <div class="cb1">
                        <input type="checkbox" name="">
                    </div>
                    <div class="cb2">
                        Saya adalah Wajib Pajak yang memperoleh penghasilan melebihi Penghasilan Tidak Kena Pajak (PTKP) namun hingga saat ini belum dapat menyerahkan NPWP kepada Bank
                    </div>

                    <br><br>

                    <div class="cb1">
                        <input type="checkbox" name="">
                    </div>
                    <div class="cb2">
                        Saya tidak diwajibkan mendaftarkan diri sebagai Wajib Pajak dengan alasan (pilih salah satu):
                    </div>

                    <div class="cbb1">
                        <div class="cb1">
                            <input type="checkbox" name="">
                        </div>
                        <div class="cb2">
                            Memiliki Penghasilan yang tidak melrbihi Penghasilan Tidak Kena Pajak (PTKP)
                        </div>

                        <div class="cb1">
                            <input type="checkbox" name="">
                        </div>
                        <div class="cb2">
                            Tidak bekerja/tidak memiliki penghasilan
                        </div>

                        <div class="cb1">
                            <input type="checkbox" name="">
                        </div>
                        <div class="cb2">
                            Lainnya <input type="text" name="">
                        </div>
                    </div>
                    Apabila di kemudian hari saya memiliki NPWP atau telah memenuhi persyaratan sebagai Wajib Pajak sesuai dengan ketentuan Peraturan      Perundang-Undangan yang berlaku. maka saya akan menyerahkan NPWP tersebut kepada Bank
                </li>
                <br>
                <li>
                    Jika terdapat perubahan data atau informasi termasuk tapi tidak terbatas perubahan data nomor telepon seluler dan/atau informasi lainnya yang telah saya berikan kepada Bank. maka saya akan memberitahukan perubahan data atau informasi tersebut kepada Bank dalam jangka waktu selambat-lambatnya 30 (tiga puluh) hari kalender terhitung sejak terjadinya perubahan perubahan tersebut atau dalam jangka waktu lain yang akan diberitahukan oleh Bank kepada Nasabah dalam bentuk dan melalui sarana apa pun.
                </li>
                <br>
                <li>
                    Setuju bahwa PT. Bank Tritech Syariah indonesia. Tbk berhak mengakhiri hubungan dengan Nasabah jika di kemudian hari dapat dibuktikan bahwa saya telah memberikan data atau informasi yang tidak benar, tidak akurat, atau tidak lengkap, atau saya tidak memberitahukan perubahan data atau informasi terkait Nasabah kepada Bank Tritech Syariah dalam jangka waktu ditentukan.
                </li>
                <br>
                <li>
                    Nasabah dengan ini menyatakan:
                    <div class="s">
                        <div class="cb1">
                            <input type="checkbox" name="">
                        </div>
                        <div class="cb2">
                            SETUJU
                        </div>
                    </div>
                    <div class="ts">
                        <div class="cb1">
                            <input type="checkbox" name="">
                        </div>
                        <div class="cb2">
                            TIDAK SETUJU
                        </div>
                    </div>
                    Bank menggunakan dan memberikan data dan/atau informasi pribadi saya kepada pihak lain yang bekerja sama dengan Bank. serta menerima informasi produk dan atau layanan melalui sarana komunikasi pribadi pada jam kerja atau di luar jam kerja.
                </li>
            </ol>
            <b>Tambahan Khusus Layanan Informasi Elektronik</b><br><br>
            <ol start="12">
                <li>Dengan ini saya bersedia mengaktifkan fasilitas roaming dari kartu telepon seluler saya untuk dapat menikmati fasilitas SMS notifikasi saat saya berada di luar negeri;</li><br>
                <li>Apabila saya tidak mengisi pilihan nominal transaksi pada layanan notifikasi yang dikehendaki untuk menerima layanan ini, maka dianggap saya       mengikuti ketentuan default layanan yang berlaku;</li><br>
                <li>
                Bersedia ditutupnya layanan notifikasi secara otomatis apabila biaya atas notifikasi tersebut tidak dapat ditagihkan kepada rekening Nasabah       dikarenakan saldo tidak cukup (di bawah saldo minimum ketentuan produk).
                </li>
            </ol>
            Dengan ditandatanganinya Fomulir Pembukaan Rekening Perorangan ini. saya menyatakan bahwa seluruh data isian dan pilihan yang saya berikan adalah benar dan saya menyatakan tunduk pada ketentuan produk/atau layanan yang berlaku berikut perubahannya di kemudian hari sebagaimana yang diberitahukan oleh PT. Bank Tritech Syariah Indonesia. Tbk melalui jaringan Kantor/surat/email/media lainnya serta tunduk dan terikat pada peraturan yang berlaku di PT. Bank Tritech Syariah Indonesia. Tbk Bank Indonesia(BI)/Otoritas Jasa Keuangan(OJK), dan fatwa Dewan Syariah Nasional(DSN) yang merupakan satu kesatuan dan bagian yang tidak terpisahkan di fomulir ini.
            <br><br>
            <div class="ttgn">
                <p style="text-align: center;">Para Pihak</p>
                <div class="tbl1">
                    <table border="1" width="45%"  align="left">
                        <tr>
                            <td style="text-align: center;">
                                <span>BANK</span><br>
                                <i>Bank</i>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <span>TANGGAL</span><br>
                                            <i>Date</i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <span>TANDA TANGAN</span><br>
                                            <i>Signature</i>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>

                <div class="tbl1">
                    <table border="1" width="45%"  align="right">
                        <tr>
                            <td style="text-align: center;">
                                <span>NASABAH</span><br>
                                <i>Costumer</i>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <span>TANGGAL</span><br>
                                            <i>Date</i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <span>TANDA TANGAN</span><br>
                                            <i>Signature</i>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
      </div>
    </div>




<!-- card 2 -->





   <div class="col-md-12 card">
      <div class="card-body">
            <h5>Untuk Keperluan Bank dan Validasi/For Bank and Validation</h5>
        <hr>

        <div class="form1">
            <div class="namaform1">
                <span>NAMA REKENING</span><br>
                <i>Account name</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NOMOR REKENINNG</span><br>
                <i>Account number</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NOMOR NASABAH</span><br>
                <i>Costumer name</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>KODE CABANG</span><br>
                <i>Branch code</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NOMOR NASABAH JOINT OR/AND</span><br>
                <i>Joint Or/And customer number</i>
            </div>
            <div class="inputform11">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>HUBUNGAN NASABAH DENGAN PIHAK BANK</span><br>
                <i>Customer relation with bank</i>
            </div>
            <div class="inputform11">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NAMA PRODUK</span><br>
                <i>Product name</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>KODE PRODUK</span><br>
                <i>Product code</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>KODE MARKETING</span><br>
                <i>Marketing code</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>KODE REFERENSI</span><br>
                <i>Refferal code</i>
            </div>
            <div class="inputform1">
                <input type="text" name="">
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NASABAH TERKAIT FATCA</span><br>
                <i>Related FATCA</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">YA <br>
                <i>Yes</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">TIDAK <br>
                <i>No</i>
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>NASABAH TERKAIT CRS &emsp;</span><br>
                <i>Related CRS</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">YA <br>
                <i>Yes</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">TIDAK <br>
                <i>No</i>
            </div>
        </div>
        <br><br><br>
        <div class="form1">
            <div class="namaform1">
                <span>PEPS</span><br>
                <i>Politically Exposed Persons</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">YA <br>
                <i>Yes</i>
            </div>
            <div class="inputform12">
                <input type="checkbox" name="">TIDAK <br>
                <i>No</i>
            </div>
            <br><br><br>

            <table border="1" width="80%">
                <tr>
                    <td style="text-align: center;" width="33.3%">
                        <span>BANK</span><br>
                        <i>Bank</i>
                    </td>
                    <td style="text-align: center;" width="33.3%">
                        <span>DISETUJUI OLEH</span><br>
                        <i>Approved by</i>
                    </td>
                    <td width="33.3%">
                        <span>Screening dan diproses oleh</span><br>
                        <i>Screening and processed by</i>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <br><br><br><br><br><br><br><br><br><br><br>
                        <p>TTD dan nama lengkap</p>
                    </td>
                    <td style="text-align: center;">
                        <br><br><br><br><br><br><br><br><br><br><br>
                        <p>TTD dan nama lengkap</p>
                    </td>
                    <td>
                        <input type="checkbox" name="">UN List/blacklist lainnya <br>
                        <input type="checkbox" name="">DHN List(Khusus Giro)
                        <br><br><br><br><br><br><br><br><br><br>
                        <p style="text-align: center;">TTD dan nama lengkap</p>
                    </td>
                </tr>
            </table>
        </div>
      </div>
    </div>
</div>